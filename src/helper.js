const helper = {
	async hasPermission () {
		try {
			const stream = await navigator.mediaDevices.getUserMedia({audio: true, video: true})
			if (!stream) {return false}
			return !!(stream.getVideoTracks().length && stream.getAudioTracks().length)
		} catch (error) {
			console.error(error)
		}
	}
}

export default helper