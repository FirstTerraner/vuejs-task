# vuejs-task

[DEMO](https://vuejs-task-nu.vercel.app/)

Libraries that I used:

- [PicoCSS](https://picocss.com/)
- [videojs-record](https://www.npmjs.com/package/videojs-record)

VueJS version: 2.7.14

## Known issues

- The recorded video can be damaged, this might be an issue with the videojs-record lib. See the screenshot below:

<img src="https://gitlab.com/FirstTerraner/vuejs-task/-/raw/master/public/issue.png">

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
